import React from 'react';
import Graph from "react-graph-vis";
import Papa from 'papaparse';
import './App.css'
const file = require('./routes.csv')



const options = {
  layout: {
    hierarchical: true
  },
  edges: {
    color: "#000000"
  },
  height: "500px"
};

const events = {
  select: function(event) {
    var { nodes, edges } = event;
  }
};

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      csvData: [],
      graph: null,
    }
  }

  populateData =(csvData) => {
    let graph = {};
    const alreadyAdded = {};
    graph.nodes = [];
    graph.edges = [];
    for (let i = 0; i < csvData.length; i++) {
      const element = csvData[i];
      
      if (!alreadyAdded[element[0]]) {
        graph.nodes.push({id: element[0], label: element[0]});
      }
      if (!alreadyAdded[element[1]]) {    
        graph.nodes.push({id: element[1], label: element[1]});
      }
      alreadyAdded[element[0]] = true;
      alreadyAdded[element[1]] = true;

    
      graph.edges.push({from:element[0], to:element[1]});
    }
    this.setState({graph})
  }

  loadCsvFile = () => {
    Papa.parse(file, {
      header: false,
      delimiter: ',',
      skipEmptyLines: true,
      download: true,
      complete: (results, file) => {
        this.setState({csvData: results.data}, () => this.populateData(this.state.csvData))
      }

    })
  }

  componentDidMount() {
    this.loadCsvFile();
  }
 
  render() {
    const {graph} = this.state;
    return graph &&  (
      <Graph
        graph={graph}
        options={options}
        events={events}
        getNetwork={network => {
          //  if you want access to vis.js network api you can set the state in a parent component using this property
        }}
      />
    );
  }

}

export default App;
